<?php
/**
 * @author Obwogo Simeon
 * @email simeon.obwogo79@gmail.com
 * @description -- Main entry class
 */

include 'app/config/config.php';
include 'app/config/AfricasTalkingGateway.php';
include 'app/session.php';
include 'app/utils.php';
include 'app/db.php';
include 'rating/rating_process.php';

class App{
	private $logger_name = 'main_app';
	private $phone;
	private $text;
	private $session_details;
	function __construct($phone, $text){
		$this->phone = $phone;
		$this->text = Utils::clean_string($text);
		Utils::set_response_headers();
	}
	public function app_serve(){
		$session = new Session($this->phone);
		if(strlen($this->text) > 0){
			// rating session
			if($session->does_rating_session_exist()){
				$session->set_session_last_time();
				return Rating::process_rating($session, $this->text);
			}
			else{
				if(strtolower($this->text) == 'start'){
					return Rating::process_rating($session, $this->text);
				}
			}
		}
	}
}
?>