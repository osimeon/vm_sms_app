<?php
/**
 * @author Obwogo Simeon
 * @email simeon.obwogo79@gmail.com
 * @description -- database class
 */
class Database{
    private static $conn;
    private static $logger_name = 'database_main';
    function __construct(){        
    }
    public static function connect(){
        self::$conn = new mysqli(HOST, USER, PASS, DATABASE);
        if(mysqli_connect_errno()){
            self::log_access(mysqli_connect_error());
        }
        return self::$conn;
    }
    private static function log_access($content){
        file_put_contents(dirname(__FILE__).'/logs/'.self::$logger_name.'_'.date('Y-m-d').'.txt', json_encode(array('time' => date('Y-m-d H:i:s'), 'content' => $content)).PHP_EOL, FILE_APPEND);
    }
}
?>