<?php
/**
 * @author Obwogo Simeon
 * @email simeon.obwogo79@gmail.com
 * @description -- app session manager
 */
class Session{
	private static $logger_name = 'app_session';
	public $phone;
	public $sessionid;
	function __construct($phone){
		$this->phone = Utils::clean_string($phone);
	}
	public function update_on_rating_survey(){
		$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$is_on_rating = 1;
		$stmt = Database::connect()->prepare("UPDATE session SET is_on_rating = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("iis", $is_on_rating, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
	}
	public function does_rating_session_exist(){
		$phone = Utils::clean_string($this->phone);
		$active = 1;
		$is_on_rating = 1;
		$stmt = Database::connect()->prepare("SELECT * FROM session WHERE phonenumber = ? AND isactive = ? AND is_on_rating = ? LIMIT 1");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("sii", $phone, $active, $is_on_rating);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
        $row = $stmt->get_result()->fetch_assoc();
        if($row != null && array_key_exists('sessionid', $row)){
        	$this->sessionid = $row['sessionid'];
        	return true;
        }
        else{
        	return false;
        }
	}
	public function create_rating_session(){
 		$rat_surv_details = $this->get_survey('shortlist_after_call_survey');
 		$rat_surv_id = $rat_surv_details['survey_id'];
 		$phone = Utils::clean_string($this->phone);
		$start = date('Y-m-d H:i:s');
		$isactive = 1;
		$stage = 1;
		$sessionid = rand(0, 100000);
		$is_on_rating = 1;

		$stmt = Database::connect()->prepare("INSERT INTO session(sessionid, survey_id, phonenumber, sessionstart, isactive, stage, is_on_rating) VALUES (?, ?, ?, ?, ?, ?, ?)");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("iissiii", $sessionid, $rat_surv_id, $phone, $start, $isactive, $stage, $is_on_rating);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
        $this->sessionid = $sessionid;
        return true;
 	}
	public function does_session_exist(){
		$phone = Utils::clean_string($this->phone);
		$active = 1;
		$stmt = Database::connect()->prepare("SELECT * FROM session WHERE phonenumber = ? AND isactive = ? LIMIT 1");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("si", $phone, $active);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
        $row = $stmt->get_result()->fetch_assoc();
        if($row != null && array_key_exists('sessionid', $row)){
        	$this->sessionid = $row['sessionid'];
        	return true;
        }
        else{
        	return false;
        }
	}
	public function get_session_details(){
		$phone = Utils::clean_string($this->phone);
		$sessionid = Utils::clean_string($this->sessionid);
		$stmt = Database::connect()->prepare("SELECT * FROM session WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("ss", $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc();
	}
 	public function create_session(){
	 	$phone = Utils::clean_string($this->phone);
		$start = date('Y-m-d H:i:s');
		$isactive = 1;
		$stage = 0;
		$sessionid = rand(0, 100000);

		$stmt = Database::connect()->prepare("INSERT INTO session(sessionid, phonenumber, sessionstart, isactive, stage) VALUES ('$sessionid', '$phone', '$start', $isactive, $stage, '$_phone')");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("issis", $sessionid, $phone, $start, $isactive, $stage);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
        $this->sessionid = $sessionid;
        return true;
 	}
 	public function set_session_last_time(){
 		$phone = Utils::clean_string($this->phone);
		$end = date('Y-m-d H:i:s');
		$sessionid = $this->sessionid;
		$stmt = Database::connect()->prepare("UPDATE session SET sessionend = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("sis", $end, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function close_all_sessions(){
 		$phone = Utils::clean_string($this->phone);
		$end = date('Y-m-d H:i:s');
		$active = 0;
		$stmt = Database::connect()->prepare("UPDATE session SET isactive = ?, sessionend = ? WHERE phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("iss", $active, $end, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function destroy_session(){
	 	$phone = Utils::clean_string($this->phone);
		$end = date('Y-m-d H:i:s');
		$sessionid = $this->sessionid;
		$active = 0;
		$stmt = Database::connect()->prepare("UPDATE session SET isactive = ?, sessionend = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("isis", $active, $end, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function update_session_survey_id($survey_id){
 		$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$stmt = Database::connect()->prepare("UPDATE session SET survey_id = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("iis", $survey_id, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function update_session_stage($stage){
	 	$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$stmt = Database::connect()->prepare("UPDATE session SET stage = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("sis", $stage, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function get_session_path(){
 		$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$stmt = Database::connect()->prepare("SELECT * FROM session WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("is", $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc()['vm_path'];
 	}
 	public function get_session_reply(){
	 	$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$stmt = Database::connect()->prepare("SELECT * FROM session WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("is", $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc()['reply'];
 	}
 	public function get_session_wrong_reply(){
	 	$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$stmt = Database::connect()->prepare("SELECT * FROM session WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("is", $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc()['wrong_reply'];
 	}
 	public function update_session_path($path){
 		$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$path = Utils::clean_string($path);
		$stmt = Database::connect()->prepare("UPDATE session SET vm_path = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("sis", $path, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function update_session_reply($reply){
	 	$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$reply = Utils::clean_string($reply);
		$stmt = Database::connect()->prepare("UPDATE session SET reply = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("sis", $reply, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function update_session_wrong_reply($reply){
	 	$phone = Utils::clean_string($this->phone);
		$sessionid = $this->sessionid;
		$reply = Utils::clean_string($reply);
		$stmt = Database::connect()->prepare("UPDATE session SET wrong_reply = ? WHERE sessionid = ? AND phonenumber = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("sis", $reply, $sessionid, $phone);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return true;
 	}
 	public function get_survey_max_questions($survey_id){
 		$stmt = Database::connect()->prepare("SELECT MAX(question_no) AS max_num FROM survey_questions WHERE survey_id = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("i", $survey_id);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc()['max_num'];
 	}
 	public function get_survey_details($survey_id){
 		$stmt = Database::connect()->prepare("SELECT * FROM survey WHERE survey_id = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("i", $survey_id);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc();
 	}
 	public function get_survey($survey_type){
 		$stmt = Database::connect()->prepare("SELECT * FROM survey WHERE survey_type = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("s", $survey_type);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc();
 	}
 	function get_menu($lang = 1, $type){
 		$stmt = Database::connect()->prepare("SELECT menu_en FROM sms_menu WHERE menu_type = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
 		$bind = $stmt->bind_param("s", $type);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
        return $stmt->get_result()->fetch_assoc()['menu_en'];
 	}
 	function get_parent_question_details($survey_id, $question){
 		$stmt = Database::connect()->prepare("SELECT * FROM survey_questions WHERE parent_survey_id = ? AND question_no = ?");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("ii", $survey_id, $question);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc();
	}
 	function get_question_details($survey_id, $question){
 		$stmt = Database::connect()->prepare("SELECT * FROM survey_questions WHERE question_no = ? AND (survey_id = ? OR parent_survey_id = ?)");
		if(!$stmt){
			self::log_access(Database::connect()->error);
			return false;
		}
        $bind = $stmt->bind_param("iii", $question, $survey_id, $survey_id);
        if(!$bind){
			self::log_access($stmt->error);
			return false;
		}
        $query = $stmt->execute();
        if(!$query){
        	self::log_access($stmt->error);
			return false;
        }
		return $stmt->get_result()->fetch_assoc();
	}
	private static function log_access($content){
		file_put_contents(dirname(__FILE__).'/logs/'.self::$logger_name.'_'.date('Y-m-d').'.txt', json_encode(array('time' => date('Y-m-d H:i:s'), 'content' => $content)).PHP_EOL, FILE_APPEND);
	}
}
?>