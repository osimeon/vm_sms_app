<?php
/**
 * @author Obwogo Simeon
 * @email simeon.obwogo79@gmail.com
 * @description -- utilities class
 */
class Utils{
	private $logger_name = 'utils_main';
	function __construct(){
	}
	public static function clean_string($string){
		return trim($string);
	}
	public static function get_elapsed_hours($end_time){
		$date1 = strtotime($end_time);
		$date2 = strtotime(date('Y-m-d H:i:s'));
		$interval = $date2 - $date1;
		$seconds = $interval % 60;
		$minutes = floor(($interval % 3600) / 60);
		return floor($interval / 3600);
	}
	public static function get_elapsed_minutes($end_time){
		$date1 = strtotime($end_time);
		$date2 = strtotime(date('Y-m-d H:i:s'));
		$interval = $date2 - $date1;
		$seconds = $interval % 60;
		$minutes = floor(($interval % 3600) / 60);
		return floor($interval / 3600);
	}
	public static function set_response_headers(){
		header('Content-type: text/plain');
	}
	public static function get_word_count($word){
		return str_word_count($word);
	}
	public static function validate_integer_decimal($input){
		return ((is_double($input) || is_numeric($input)) && ($input >= 0)) ? true : false;
	}
	public static function validate_one_to_three($input){
		return (is_numeric($input)) && (array_key_exists($input, array('1' => 1, '2' => 2, '3' => 3))) ? true : false;
	}
	public static function validate_integer($input){
		return is_numeric($input);
	}
	public static function validate_yes_no_la_ndio($input){
		$response = array('yes', 'no', 'ndio', 'la');
		for($i = 0; $i < sizeof($response); $i++){
			if(strtolower($input) == $response[$i]){
				return true;
			}
		}
		return false;
	}
	public static function validate_valid_date($date){
		$n_date = str_replace(' ', '', trim($date));
		$curr_year = date('Y');
		// check if date has slashes
		if(strpos($date, '/') > 0){
			$t_date = str_replace('/', ',', $n_date);
			$d_arr = explode(',', $t_date);
			if(sizeof($d_arr) == 3){
				$day = $d_arr[0];
				$month = $d_arr[1];
				$year = $d_arr[2];
				if($year > $curr_year){
					return false;
				}
				return checkdate($month, $day, $year);
			}
		}
		if(strpos($date, '-') > 0){
			$t_date = str_replace('-', ',', $n_date);
			$d_arr = explode(',', $t_date);
			if(sizeof($d_arr) == 3){
				$day = $d_arr[0];
				$month = $d_arr[1];
				$year = $d_arr[2];
				if($year > $curr_year){
					return false;
				}
				return checkdate($month, $day, $year);
			}
		}
		return false;
	}
	public static function validate_first_part_num($input){
		$array = explode('*', $input);
		return self::validate_integer_decimal($array[0]);
	}
	public static function update_session_path($session, $content){
		$path = !empty($session->get_session_path()) ? json_decode($session->get_session_path(), true) : array();
		$path[] = $content;
		$session->update_session_path(json_encode($path));
	}
	private function log_access($content){
		file_put_contents('logs/'.$this->logger_name.'_'.date('Y-m-d').'.txt', json_encode(array('time' => date('Y-m-d H:i:s'), 'content' => $content)).PHP_EOL, FILE_APPEND);
	}
}
?>