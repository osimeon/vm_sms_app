<?php
/**
 * @author Obwogo Simeon
 * @email simeon.obwogo79@gmail.com
 * @description -- rating survey manager
 */
class Rating{
	private static $logger_name = 'rating_survey_manager';
	private static $session;
	function __construct(){
	}
	public static function process_rating($session, $content){
		self::$session = $session;
		if(!$session->does_session_exist()){
			$session->create_rating_session();
			$session_details = $session->get_session_details();
			$session_survey_id = $session_details['survey_id'];
			$survey_question_details = $session->get_question_details($session_survey_id, 1);
			$session->set_session_last_time();
			// log path
			Utils::update_session_path($session, 'rating');
			return $survey_question_details['question'];
		}
		else{
			$session_details = $session->get_session_details();
			$session_survey_id = $session_details['survey_id'];
			$session_survey_id = ($session_survey_id > 0) ? $session_survey_id : $session_details['parent_survey_id'];
			$session_stage = $session_details['stage'];
			$survey_last_question = $session->get_survey_max_questions($session_survey_id);

			$current_question_details = $session->get_question_details($session_survey_id, $session_stage);
			$allowed_types = $current_question_details['question_allowed'];
			$skip_pattern = $current_question_details['question_skip_pattern'];

			// check if response is in allowed types
			if(strlen($allowed_types) > 0){
				$allowed_types_arr = explode(',', $allowed_types);
				if(!in_array($content, $allowed_types_arr)){
					$surv_num_ques_num = $session_survey_id.'.'.$session_stage;
					$reply = !empty($session->get_session_wrong_reply()) ? json_decode($session->get_session_wrong_reply(), true) : array();
					$reply[] = $surv_num_ques_num.' '.Utils::clean_string($content);
					$session->update_session_wrong_reply(json_encode($reply));
					return $current_question_details['question_error_message'];
				}
			}

			if((strlen($skip_pattern) == 0) && ($session_stage == $survey_last_question)){
				$surv_num_ques_num = $session_survey_id.'.'.$session_stage;
				$survey_details = $session->get_survey_details($session_survey_id);
				$reply = !empty($session->get_session_reply()) ? json_decode($session->get_session_reply(), true) : array();
				$reply[] = $surv_num_ques_num.' '.Utils::clean_string($content);
				$session->update_session_reply(json_encode($reply));
				Utils::update_session_path($session, $reply);
				$session->set_session_last_time();
				$session->close_all_sessions();
				return $survey_details['survey_remarks'];
			}
			else{
				// survey num, question num
				$surv_num_ques_num = $session_survey_id.'.'.$session_stage;
				$reply = !empty($session->get_session_reply()) ? json_decode($session->get_session_reply(), true) : array();
				$reply[] = $surv_num_ques_num.' '.Utils::clean_string($content);
				$session->update_session_reply(json_encode($reply));
				Utils::update_session_path($session, $reply);
				$session->set_session_last_time();

				if(strlen($skip_pattern) > 0){
					if($skip_pattern == 'goto_212'){
						$survey_next_question_no = '212';
						$next_question_details = $session->get_question_details($session_survey_id, $survey_next_question_no);
						$response = $next_question_details['question'];
						$session->update_session_stage('212');
						return $response;
					}
					if($skip_pattern == 'goto_3'){
						$survey_next_question_no = '3';
						$next_question_details = $session->get_question_details($session_survey_id, $survey_next_question_no);
						$response = $next_question_details['question'];
						$session->update_session_stage('3');
						return $response;
					}
					else{
						$pattern_arr = json_decode($skip_pattern, true);
						switch($pattern_arr[strtolower($content)]){
							case 'skip_21':
								$survey_next_question_no = '21';
								$next_question_details = $session->get_question_details($session_survey_id, $survey_next_question_no);
								$response = $next_question_details['question'];
								$session->update_session_stage('21');
								// die('ending at...'.$content.' matching...skip_21..next_stage_being...'.$survey_next_question_no);
								return $response;
								break;
							case 'skip_22':
								// die('ending at...'.$content.' matching...skip_22');
								$survey_next_question_no = '22';
								$next_question_details = $session->get_question_details($session_survey_id, $survey_next_question_no);
								$response = $next_question_details['question'];
								$session->update_session_stage('22');
								return $response;
								break;
							case 'skip_211':
								// die('ending at...'.$content.' matching...211');
								$survey_next_question_no = '211';
								// die('question id...'.$survey_next_question_no.' with survey id...'.$session_survey_id);
								$next_question_details = $session->get_question_details($session_survey_id, $survey_next_question_no);
								$response = $next_question_details['question'];
								// die($response);
								$session->update_session_stage('211');
								return $response;
								break;
						}
					}
					// die('ending...');
				}
				// die('why are we here...with pattern...'.$skip_pattern.' and question no...'.$session_stage);
				$survey_next_question_no = $session_stage + 1;
				$next_question_details = $session->get_question_details($session_survey_id, $survey_next_question_no);
				$response = $next_question_details['question'];
				$session->update_session_stage($survey_next_question_no);
				return $response;
			}
		}
	}
	private static function log_access($content){
		file_put_contents(dirname(__FILE__).'/logs/'.self::$logger_name.'_'.date('Y-m-d').'.txt', json_encode(array('time' => date('Y-m-d H:i:s'), 'content' => $content)).PHP_EOL, FILE_APPEND);
	}
}
?>