<?php
include '../app.php';
$message = trim($_POST["message"]);
$obj = new App($_POST['from'], $message);
$response = $obj->app_serve();
$message = urlencode($response);

if(isset($_POST['is_fake_form']))
	header("Location: sms_form.php?response=$message");
else
	header("Location: form.php?response=$message");
?>